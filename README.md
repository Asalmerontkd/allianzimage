# Plugin

### Descripción

**name:** allianzImage

**version:** 1.2.2

**description:** Módulo de fotografía para documentos con detección de bordes y corrección de perspectiva.

**id:** io-mariachi-allianz-imagen

**platforms:** android, ios

**android abi:** x86, x86_64, armeabi, armeabi-v7a, arm64-v8a, mips, mips64

### Instalación

Para la instalación del plugin se puede realizar desde la URL de bitbucket, para usuarios con privilegios, con los siguientes pasos:

```sh
$ cd proyectoIonic
$ ionic platform add (android or ios)
$ ionic plugin add https://Asalmerontkd@bitbucket.org/Asalmerontkd/allianzimage.git
```

O clonando el repositorio

```sh
$ cd proyectoIonic
$ ionic platform add (android or ios)
$ ionic plugin add /ruta/al/plugin/allianzImage
```

### Uso

Ejemplo de implementación en el código

```html
<...>
    <div class="button-bar">
        <a class="button" onclick="allianzImage.imageMethod('CARTILLA SERVICIO MILITAR', funcionOk, funcionError);"></a>
    </div>
</...>
```

Ejemplo de implementación para los eventos onSuccess y onError

```JavaScript
function funcionOk(response)
{
    /*
        TODO aquí hacer lo que sea con base 64, ejemplo:
            response.image          IMAGEN EN TAMAÑO COMPLETO
            response.thumbImage     IMAGEN EN TAMAÑO DE 150 PX
    */
    var datos = "data:image/jpg;base64," + response.image;
    document.getElementById('imgID').src = datos;
    var datosSmall = "data:image/jpg;base64," + response.thumbImage;
    document.getElementById('imgScaledID').src = datosSmall;
}

function funcionError(response)
{
    //TODO aquí muestra algún error como evento cancelado
}
```

**NOTA** para Android es necesario contar con la variable de entorno "ANDROID_NDK_HOME", la cual puedes exportar con la siguiente linea en tu terminal:

**NOTA** es necesario tener instalado CMake y Android NDK, en caso de no tenerlo instalado se puede descargar desde el SDK Manager, en el apartado de SDK Tools, de Android Studio.

```sh
export ANDROID_NDK_HOME=$ANDROID_HOME/ndk-bundle
```

**NOTA** Para un correcto funcionamiento en todos los dispositivos se debe instalar un apk deacuerdo a la arquitectura del procesador, pueden ser procesadores armeabi, armeabi-v7a, mips y x86. Estos se generan con la siguiente instrucción:

```sh
ionic build android
```


[![N|Mariachi IO](http://res.cloudinary.com/dkavl9sap/image/upload/v1501117238/mariachiLogoBLK_vxsp0t.png)](http://res.cloudinary.com/dkavl9sap/image/upload/v1501117238/mariachiLogoBLK_vxsp0t.png)

