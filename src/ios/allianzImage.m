/********* allianzImage.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "MMCameraPickerController.h"

@interface allianzImage : CDVPlugin {
}

- (void)imageMethod:(CDVInvokedUrlCommand*)command;
@end

@implementation allianzImage

- (void)imageMethod:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];

    UIStoryboard * scannerStoryboard = [UIStoryboard storyboardWithName:@"Scanner" bundle:NULL];

    MMCameraPickerController * ScannerViewController = [scannerStoryboard instantiateViewControllerWithIdentifier:@"scannerController"];
    ScannerViewController.pluginInstance = self;
    ScannerViewController.command = command;
    [self.viewController presentViewController:ScannerViewController animated:YES completion:^{}];
}

@end
