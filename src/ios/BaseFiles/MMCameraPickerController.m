
#import "MMCameraPickerController.h"
#import <AVFoundation/AVFoundation.h>
#import <Cordova/CDV.h>
#define backgroundHex @"2196f3"
@interface MMCameraPickerController ()<UIGestureRecognizerDelegate, MMCropDelegate>{
    CGFloat screenWidth;
    CGFloat screenHeight;
    CGFloat topX;
    CGFloat topY;
    BOOL isImageResized;
    BOOL isSaveWaitingForResizedImage;
    BOOL isRotateWaitingForResizedImage;
    BOOL isCapturingImage;
    float effectiveScale ,beginGestureScale;
}
@property (strong, nonatomic) AVCaptureSession * mySesh;

@property (strong, nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (strong, nonatomic) AVCaptureDevice * myDevice;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer * captureVideoPreviewLayer;
@property (strong, nonatomic) UIView * imageStreamV;
@property (strong, nonatomic) UIImageView * capturedImageV;
@property (strong, nonatomic) UIImage * capturedImage;
@property (strong,nonatomic) CameraFocusSquare *camFocus;
@property (weak, nonatomic) IBOutlet UINavigationBar *navbar;

@end

@implementation MMCameraPickerController
BOOL viewHasntLoaded = true;
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    if ( [gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]] ) {
        beginGestureScale = effectiveScale;
    }
    return YES;
}
-(IBAction)closeView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(void)viewDidLoad
{
    [super viewDidLoad];
    effectiveScale=1;
    [self setUI];
    self.captureBut.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [UIView animateWithDuration:0.3 animations:^{
        self.switchCameraBut.alpha=1;
        self.flashBut.alpha=0;
        self.retakeBut.alpha=0;
        self.doneBut.alpha=0;
    }];


    UIBarButtonItem * navBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(closeView:)];
    self.navbar.tintColor = UIColor.whiteColor;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navbar.topItem.leftBarButtonItem = navBarButton;
}



- (void) viewDidAppear:(BOOL)animated {

    [super viewDidAppear:animated];
    if (viewHasntLoaded) {
        viewHasntLoaded = false;
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
            [self setUpCameraFoundation];
        }
        else {
            // iOS 8
            AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            switch (status) {
                case AVAuthorizationStatusDenied:
                case AVAuthorizationStatusRestricted:
                    NSLog(@"SC: Not authorized, or restricted");
                    break;
                case AVAuthorizationStatusAuthorized:
                    [self setUpCameraFoundation];
                    break;
                case AVAuthorizationStatusNotDetermined: {
                    // not determined
                    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                        if(granted){
                            [self setUpCameraFoundation];
                        } else {
                        }
                    }];
                }
                default:
                    break;
            }
        }
        _backBut.hidden = true;
        _zoomSlider.hidden = true;
        [self.view bringSubviewToFront:self.bottomView];
        [self.view bringSubviewToFront:self.backBut];
    }
}

-(void)setUI{}

#pragma mark Camera Delegate
-(void)didFinishCaptureImage:(UIImage *)capturedImage withMMCam:(MMCameraPickerController*)cropcam{

    [cropcam closeWithCompletion:^{
        NSLog(@"dismissed");
        if(capturedImage!=nil){
            CropViewController *crop=[self.storyboard instantiateViewControllerWithIdentifier:@"crop"];
            crop.pluginInstance = self.pluginInstance;
            crop.command = self.command;
            crop.cropdelegate=self;
            crop.cameraInstance = self;
            crop.adjustedImage=capturedImage;
            [self presentViewController:crop animated:NO completion:^{

            }];
        }
    }];


}
-(void)authorizationStatus:(BOOL)status{

}

#pragma mark crop delegate
-(void)didFinishCropping:(UIImage *)finalCropImage from:(CropViewController *)cropObj{


    [cropObj closeWithCompletion:^{
    }];
    NSLog(@"Size of Image %lu",(unsigned long)UIImageJPEGRepresentation(finalCropImage, 0.5).length);
    [_imageStreamV removeFromSuperview];
    _imageStreamV = NULL;
    viewHasntLoaded = true;
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)setUpCameraFoundation{

    if (_imageStreamV == nil) _imageStreamV = [[UIView alloc]init];
    _imageStreamV.alpha = 1;
    _imageStreamV.frame = self.view.bounds;
    [self.view addSubview:_imageStreamV];
    [self.view sendSubviewToBack:_imageStreamV];
    if (_mySesh == nil) _mySesh = [[AVCaptureSession alloc] init];
    _mySesh.sessionPreset = AVCaptureSessionPresetPhoto;
    _captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_mySesh];
    _captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _captureVideoPreviewLayer.frame = _imageStreamV.layer.bounds;
    [_imageStreamV.layer addSublayer:_captureVideoPreviewLayer];
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    if (devices.count==0) {
        NSLog(@"SC: No devices found (for example: simulator)");
        return;
    }
    _myDevice = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo][0];
    if ([_myDevice isFlashAvailable] && _myDevice.flashActive && [_myDevice lockForConfiguration:nil]) {
        _myDevice.flashMode = AVCaptureFlashModeOff;
        [_myDevice unlockForConfiguration];
    }
    NSError * error = nil;
    AVCaptureDeviceInput * input = [AVCaptureDeviceInput deviceInputWithDevice:_myDevice error:&error];
    [_mySesh addInput:input];

    _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary * outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [_stillImageOutput setOutputSettings:outputSettings];
    [_mySesh addOutput:_stillImageOutput];
    [_mySesh startRunning];
    _captureVideoPreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    if (_capturedImageV == nil) _capturedImageV = [[UIImageView alloc]init];
    _capturedImageV.frame = _imageStreamV.frame; // just to even it out
    _capturedImageV.backgroundColor = [UIColor clearColor];
    _capturedImageV.userInteractionEnabled = YES;
    _capturedImageV.clipsToBounds=YES;
    _capturedImageV.contentMode = UIViewContentModeScaleAspectFill;
    [self.view insertSubview:_capturedImageV aboveSubview:_imageStreamV];

    UIPinchGestureRecognizer * pinchZoom = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinchGesture:)];
    pinchZoom.delegate=self;
    [_capturedImageV addGestureRecognizer:pinchZoom];
}

#pragma mark TAP TO FOCUS

- (void) tapSent:(CGPoint)sender {

    if (_capturedImageV.image == nil) {
        CGPoint aPoint = sender;
        if (_myDevice != nil) {
            if([_myDevice isFocusPointOfInterestSupported] &&
               [_myDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
                double pX = aPoint.x / _imageStreamV.bounds.size.width;
                double pY = aPoint.y / _imageStreamV.bounds.size.height;
                double focusX = pY;
                double focusY = 1 - pX;
                if([_myDevice isFocusPointOfInterestSupported] && [_myDevice isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {

                    if([_myDevice lockForConfiguration:nil]) {
                        [_myDevice setFocusPointOfInterest:CGPointMake(focusX, focusY)];
                        [_myDevice setFocusMode:AVCaptureFocusModeAutoFocus];
                        [_myDevice setExposurePointOfInterest:CGPointMake(focusX, focusY)];
                        [_myDevice setExposureMode:AVCaptureExposureModeContinuousAutoExposure];
                    }
                    [_myDevice unlockForConfiguration];
                }
            }
        }
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:touch.view];
    [self tapSent:touchPoint];
    if (_camFocus){
        [_camFocus removeFromSuperview];
    }
    if (YES){
        _camFocus = [[CameraFocusSquare alloc]initWithFrame:CGRectMake(touchPoint.x-40, touchPoint.y-40, 80, 80)];
        [_camFocus setBackgroundColor:[UIColor clearColor]];
        [_imageStreamV addSubview:_camFocus];
        [_camFocus setNeedsDisplay];

        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:3.0];
        [_camFocus setAlpha:0.0];
        [UIView commitAnimations];
    }
}

- (void) closeWithCompletion:(void (^)(void))completion {
    [self dismissViewControllerAnimated:YES completion:^{
        completion();
        [_mySesh stopRunning];
        _mySesh = nil;
        _capturedImageV.image = nil;
        [_capturedImageV removeFromSuperview];
        _capturedImageV = nil;
        [_imageStreamV removeFromSuperview];
        _imageStreamV = nil;
        _stillImageOutput = nil;
        _myDevice = nil;
        self.view = nil;
        _camdelegate = nil;
        [self removeFromParentViewController];
    }];
}

- (void) capturePic {
    if (isCapturingImage) {
        return;
    }
    isCapturingImage = YES;
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in _stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    if(videoConnection!=nil){
        [videoConnection setVideoScaleAndCropFactor:effectiveScale];
    }
    [_stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         if(!CMSampleBufferIsValid(imageSampleBuffer)){
             return;
         }
         [_mySesh stopRunning];
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         UIImage * capturedImage = [[UIImage alloc]initWithData:imageData scale:1];
         NSLog(@"%lu",(unsigned long)UIImageJPEGRepresentation(capturedImage, 1.0).length);
         if (_myDevice == [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo][0]) {
             self.capturedImage = capturedImage;
         }

         else if (_myDevice == [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo][1]) {
         }

         isCapturingImage = NO;
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
             [self retakeAction:self];
             [self doneAction:self];
         }];

         imageData = nil;
     }];
}


- (IBAction)capturePhoto:(id)sender {
    [self capturePic];
    [UIView animateWithDuration:0.3 animations:^{
        self.switchCameraBut.alpha=0;
        self.retakeBut.alpha=1;
        self.doneBut.alpha=1;

    }];
}

- (IBAction)retakeAction:(id)sender {


    isCapturingImage = false;
    [_mySesh startRunning];
    _capturedImageV.image=nil;
    [UIView animateWithDuration:0.3 animations:^{
        self.switchCameraBut.alpha=1;
        self.flashBut.alpha=0;
        self.retakeBut.alpha=0;
        self.doneBut.alpha=0;

    }];

}

- (IBAction)doneAction:(id)sender {
    if(self.capturedImage!=nil){
        UIStoryboard * scannerStoryboard = [UIStoryboard storyboardWithName:@"Scanner" bundle:NULL];
        CropViewController *crop=[scannerStoryboard instantiateViewControllerWithIdentifier:@"crop"];
        crop.pluginInstance = self.pluginInstance;
        crop.command = self.command;
        crop.adjustedImage=_capturedImage;
        crop.cropdelegate=self;
        crop.cameraInstance = self;

        [self presentViewController:crop animated:YES completion:nil];
    }
}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)recognizer
{

    effectiveScale = beginGestureScale * recognizer.scale;

    if (effectiveScale < 1.0){
        effectiveScale = 1.0;
    }

    else if (effectiveScale > 5){
        effectiveScale = 5;
    }

    else{
    }



    [CATransaction begin];

    [CATransaction setAnimationDuration:.025];

    [_captureVideoPreviewLayer setAffineTransform:CGAffineTransformMakeScale(effectiveScale, effectiveScale)];

    [CATransaction commit];

    [UIView animateWithDuration:0.1 animations:^{
        self.zoomSlider.value = effectiveScale/5;

    }];
    NSLog(@"%f %f",recognizer.scale,[[_stillImageOutput connectionWithMediaType:AVMediaTypeVideo] videoScaleAndCropFactor]);



}
- (IBAction)zoomsliderAction:(UISlider *)sender {
    [self slideZoomINOUT:sender.value];
}

- (IBAction)flashAction:(id)sender {
    if ([_myDevice hasTorch] && [_myDevice position]==AVCaptureDevicePositionBack) {
        [_myDevice lockForConfiguration:nil];

        if(toogleFlash){
            [_myDevice setTorchMode:AVCaptureTorchModeOff];
        }
        else{
            [_myDevice setTorchMode:AVCaptureTorchModeOn];
        }

        [_myDevice unlockForConfiguration];
    }

    toogleFlash=!toogleFlash;
}

- (IBAction)switchCameraAction:(id)sender {
    AVCaptureDevicePosition desiredPosition;
    if (toggleCamera){
        [self.flashBut setEnabled:YES];
        desiredPosition = AVCaptureDevicePositionBack;
    }

    else{

        [self.flashBut setEnabled:NO];
        toogleFlash=NO;
        [_myDevice lockForConfiguration:nil];
        [_myDevice setTorchMode:AVCaptureTorchModeOff];
        [_myDevice unlockForConfiguration];
        desiredPosition = AVCaptureDevicePositionFront;
    }

    for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
        if ([d position] == desiredPosition) {
            [[_captureVideoPreviewLayer session] beginConfiguration];
            AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:d error:nil];
            for (AVCaptureInput *oldInput in [[_captureVideoPreviewLayer session] inputs]) {
                [[_captureVideoPreviewLayer session] removeInput:oldInput];
            }
            [[_captureVideoPreviewLayer session] addInput:input];
            [[_captureVideoPreviewLayer session] commitConfiguration];
            break;
        }
    }
    toggleCamera = !toggleCamera;
}

- (IBAction)backToParent:(id)sender {
    [self.camdelegate didFinishCaptureImage:nil withMMCam:self];
}

-(void)slideZoomINOUT:(CGFloat)scale{
    effectiveScale = scale*5;

    if (effectiveScale < 1.0)

        effectiveScale = 1.0;

    if (effectiveScale > 5)

        effectiveScale = 5;

    [CATransaction begin];

    [CATransaction setAnimationDuration:.025];

    [_captureVideoPreviewLayer setAffineTransform:CGAffineTransformMakeScale(effectiveScale, effectiveScale)];

    [CATransaction commit];

}

#pragma mark Latest Photo
- (void)latestPhotoWithCompletion:(void (^)(UIImage *photo))completion
{

    ALAssetsLibrary *library=[[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        [group setAssetsFilter:[ALAssetsFilter allPhotos]];
        if ([group numberOfAssets] > 0) {
            [group enumerateAssetsAtIndexes:[NSIndexSet indexSetWithIndex:[group numberOfAssets]-1] options:0
                                 usingBlock:^(ALAsset *alAsset, NSUInteger index, BOOL *innerStop) {
                                     if (alAsset) {
                                         ALAssetRepresentation *representation = [alAsset defaultRepresentation];
                                         UIImage *img = [UIImage imageWithCGImage:[representation fullScreenImage]];
                                         completion(img);
                                         *innerStop = YES;
                                     }
                                 }];
        }
    } failureBlock: ^(NSError *error) {
    }];


}

- (NSString *)directory
{
    NSMutableString *path = [NSMutableString new];
    [path appendString:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
    [path appendString:@"/Images/"];

    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:&error];

        if (error) {
            return nil;
        }
    }

    return path;
}

- (NSURL *)saveJPGImageAtDocumentDirectory:(UIImage *)image {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd_HH:mm:SSSSZ"];

    NSString *directory = [self directory];

    if (!directory) {

        return nil;
    }

    NSString *fileName = [[dateFormatter stringFromDate:[NSDate date]] stringByAppendingPathExtension:@"jpg"];
    NSString *filePath = [directory stringByAppendingString:fileName];

    if (filePath == nil) {

        return nil;
    }

    NSData *data = UIImageJPEGRepresentation(image, 1);
    [data writeToFile:filePath atomically:YES];

    NSURL *assetURL = [NSURL URLWithString:filePath];

    NSLog(@"%@",assetURL);
    return assetURL;
}
- (void)saveImage: (UIImage*)image
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          @"test.png" ];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
}

- (UIImage*)loadImage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

@end
