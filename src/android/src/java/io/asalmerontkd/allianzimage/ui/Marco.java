package io.asalmerontkd.allianzimage.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import io.asalmerontkd.allianzimage.utils.Constants;

/**
 * Created by asalmerontkd on 02/05/18.
 */

public class Marco extends View  {

    public Marco(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint myPaint = new Paint();

        int regx = canvas.getWidth() / 9; //rejilla de 9
        int ancho = regx * 6;
        int canty = canvas.getHeight() / regx;
        int regy = canvas.getHeight() / canty; // rejilla alto
        int alto = regy * 9;

        int espacioy = ((canvas.getHeight() - alto) / 2) - (regy / 2);
        int espaciox = (canvas.getWidth() - ancho) / 2;

        int barrax = ancho / 3;
        int barray = alto / 3;

        Constants.x = barrax;
        Constants.y = barray;
        Constants.alto = alto;
        Constants.ancho = ancho;
        Constants.rejilla = 9;
        Constants.constanteX = 6;
        Constants.constanteY = 9;

        Paint myGrayPaint = new Paint();
        //getResources().getIdentifier("black", "layout", getPackageName()
        myGrayPaint.setColor(Color.parseColor("#000000"));
        myGrayPaint.setAlpha(35);

        canvas.drawRect(0, 0, canvas.getWidth(), espacioy, myGrayPaint);
        canvas.drawRect(0, espacioy, espaciox, canvas.getHeight(), myGrayPaint);
        canvas.drawRect(espaciox, alto + espacioy, canvas.getWidth(), canvas.getHeight(),
                myGrayPaint);
        canvas.drawRect(ancho + espaciox, espacioy, canvas.getWidth(), alto + espacioy,
                myGrayPaint);

        myPaint.setColor(Color.parseColor("#58E2C2"));
        myPaint.setStrokeWidth(10);

        canvas.drawPoint(espaciox, espacioy, myPaint); //punto superior izquierdo
        canvas.drawPoint(ancho + espaciox, espacioy, myPaint); //punto superior derecho
        canvas.drawPoint(espaciox, alto + espacioy, myPaint); //punto inferior izquierdo
        canvas.drawPoint(ancho + espaciox, espacioy + alto, myPaint); //punto inferior derecho

        canvas.drawLine(espaciox, espacioy, espaciox + (barrax / 2), espacioy, myPaint);
        canvas.drawLine((ancho + espaciox) - (barrax / 2), espacioy, ancho + espaciox, espacioy,
                myPaint);

        canvas.drawLine(espaciox, alto + espacioy, espaciox + (barrax / 2), espacioy + alto,
                myPaint);
        canvas.drawLine((ancho + espaciox) - (barrax / 2), alto + espacioy, ancho + espaciox,
                espacioy + alto, myPaint);

        canvas.drawLine(espaciox, espacioy, espaciox, espacioy + (barrax / 2), myPaint);
        canvas.drawLine(espaciox, (alto + espacioy) - (barrax / 2), espaciox, alto + espacioy,
                myPaint);

        canvas.drawLine(ancho + espaciox, espacioy, ancho + espaciox, espacioy + (barrax / 2),
                myPaint);
        canvas.drawLine(ancho + espaciox, (alto + espacioy) - (barrax / 2), ancho + espaciox,
                espacioy + alto, myPaint);


    }
}
