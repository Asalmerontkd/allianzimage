package io.asalmerontkd.allianzimage.ui.bordes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;

import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;

import io.asalmerontkd.allianzimage.utils.OrderPoints;

/**
 * Created by Ken on 07/06/17.
 */

public class ImageViewAdjustBorders extends View {
    
        /**
         * Default radious for circles
         */
        private int RADIOUS = 45;
    
        /**
         * List containts circles to paint
         */
        private List<CircleArea> mCircles = new ArrayList();
    
        /**
         * List containts touched circles
         */
        private SparseArray<CircleArea> mCirclePointer = new SparseArray<CircleArea>();
    
        /**
         * Paint with styles for draw circles
         */
        private Paint mCirclePaint;
    
        /**
         * Paint for draw lines between circles
         */
        private Paint mLineConectorPaint;
    
    
        //drawing path
        private Path drawPath;
        //drawing and canvas paint
        private Paint canvasPaint;
    
        private Paint mCircleWhite;
    
    
        /**
         * canvas for image and points
         */
        private Canvas drawCanvas;
        //canvas bitmap
        private Bitmap canvasBitmap;
    
    
        //Image photo resizing
        private Bitmap bitmapPhoto;
    
        OrderPoints orderPoints;
    
        public ImageViewAdjustBorders(Context context) {
            super(context);
        }
    
        public ImageViewAdjustBorders(Context context, @Nullable AttributeSet attrs) {
            super(context, attrs);
        }
    
        public ImageViewAdjustBorders(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }
    
        public ImageViewAdjustBorders(Context context, Bitmap bitmap) {
            super(context);
            this.bitmapPhoto = bitmap;
            setupDrawing();
        }
    
        public void setRADIOUS(int RADIOUS) {
            this.RADIOUS = RADIOUS;
        }
    
        /**
         * Setup initial drawing canvas and paths
         */
        private void setupDrawing() {
            /**
             * Bitmap path and canvas
             */
            canvasPaint = new Paint();
            drawPath = new Path();
            orderPoints = new OrderPoints();
    
            //Circles configuration
            mCirclePaint = new Paint();
    
            mCirclePaint.setColor(Color.parseColor("#58E2C2"));
            mCirclePaint.setStrokeWidth(5);
            mCirclePaint.setStyle(Paint.Style.STROKE);
    
            //Inicializate line paint
            mLineConectorPaint = new Paint();
            mLineConectorPaint.setColor(Color.parseColor("#58E2C2"));
            mLineConectorPaint.setStrokeWidth(5);
            mLineConectorPaint.setStyle(Paint.Style.FILL);
    
            mCircleWhite = new Paint();
            mCircleWhite.setColor(Color.parseColor("#FFFFFF"));
            mCircleWhite.setAlpha(480);
            mCircleWhite.setStyle(Paint.Style.FILL);
            //setAlpha(0.9f);
    
        }
    
    
        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
    
            canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
    
            if (bitmapPhoto != null) {
                //Draw image
                drawCanvas = new Canvas(bitmapPhoto);
            }
        }
    
        /**
         * Method for draw on canvas
         *
         * @param canvas
         */
        @Override
        protected void onDraw(Canvas canvas) {
    
            super.onDraw(canvas);
    
            if (bitmapPhoto != null) {
                canvas.drawBitmap(bitmapPhoto, 0, 0, canvasPaint);
            }
            canvas.drawPath(drawPath, canvasPaint);
            /**
             * Draw all lines between circles
             */
            ArrayList<Point> pointsCircles = new ArrayList<Point>();
            for (int i = 0; i < mCircles.size(); i++){
                CircleArea currentCircle = mCircles.get(i);
                pointsCircles.add(new Point(currentCircle.centerX, currentCircle.centerY));
            }
            Point[] coordinates = new Point[0];
            if (pointsCircles.size()>0)
            {
                coordinates = orderPoints.sortPoints(pointsCircles);
            }
            for (int i = 0; i < coordinates.length; i++) {
                Point currentCircle = coordinates[i];
                Point nextCircle;
                if (i == coordinates.length - 1) {
                    nextCircle = coordinates[0];
                } else {
                    nextCircle = coordinates[i + 1];
                }
                canvas.drawLine((float)currentCircle.x, (float)currentCircle.y, (float)nextCircle.x, (float)nextCircle.y, mLineConectorPaint);
            }
            /**
             * Draw all circles
             */
            for (CircleArea circle : mCircles) {
                RADIOUS = circle.radius / 2;
                if (circle.centerX + circle.radius < circle.radius) {
                    circle.centerX = RADIOUS;
                }
                if (circle.centerX + circle.radius >= drawCanvas.getWidth()) {
                    circle.centerX = drawCanvas.getWidth() - RADIOUS;
                }
                if (circle.centerY + circle.radius < circle.radius) {
                    circle.centerY = RADIOUS;
                }
                if (circle.centerY + circle.radius >= drawCanvas.getHeight()) {
                    circle.centerY = drawCanvas.getHeight() - RADIOUS;
                }
                canvas.drawCircle(circle.centerX, circle.centerY, circle.radius, mCircleWhite);
                canvas.drawCircle(circle.centerX, circle.centerY, circle.radius, mCirclePaint);
    
            }
    
    
    
    
        }
    
        public void drawAllCircles() {
            /**
             * Draw all circles
             */
            for (CircleArea circle : mCircles) {
                drawCanvas.drawCircle(circle.centerX, circle.centerY, circle.radius, mCirclePaint);
                drawCanvas.drawCircle(circle.centerX, circle.centerY, circle.radius - 10, mLineConectorPaint);
            }
    
            /**
             * Draw all lines between circles
             */
            for (int i = 0; i < mCircles.size(); i++) {
                CircleArea currentCircle = mCircles.get(i);
                CircleArea nextCircle;
                if (i == mCircles.size() - 1) {
                    nextCircle = mCircles.get(0);
                } else {
                    nextCircle = mCircles.get(i + 1);
                }
                drawCanvas.drawLine(currentCircle.centerX, currentCircle.centerY,
                        nextCircle.centerX, nextCircle.centerY, mLineConectorPaint);
            }
    
    
        }
    
    
        /**
         * Add circles for list and invalidate for re draw canvas.
         *
         * @param circleAreaList
         */
        public void setCirclesBorders(ArrayList<Point>  circleAreaList) {
    
    
            //TODO set circleAreal list to mCircles
            mCircles = new ArrayList();
            if (circleAreaList.isEmpty() || circleAreaList==null) {
                Log.e("imageView", "Is null corner");
                mCircles.add(new CircleArea(drawCanvas.getWidth() / 4, drawCanvas.getHeight() / 4, RADIOUS));
                mCircles.add(new CircleArea(drawCanvas.getWidth() / 2 + drawCanvas.getWidth() / 4, drawCanvas.getHeight() / 4, RADIOUS));
    
                mCircles.add(new CircleArea(drawCanvas.getWidth() / 2 + drawCanvas.getWidth() / 4, drawCanvas.getHeight() / 2 + drawCanvas.getHeight() / 4, RADIOUS));
                mCircles.add(new CircleArea(drawCanvas.getWidth() / 4, drawCanvas.getHeight() / 2 + drawCanvas.getHeight() / 4, RADIOUS));
                invalidate();
            }
            else {
                Log.e("imageView", "IS OK corner");
                mCircles.add(new CircleArea((int) circleAreaList.get(0).x, (int) circleAreaList.get(0).y, RADIOUS));
                mCircles.add(new CircleArea((int) circleAreaList.get(1).x, (int) circleAreaList.get(1).y, RADIOUS));
                mCircles.add(new CircleArea((int) circleAreaList.get(2).x, (int) circleAreaList.get(2).y, RADIOUS));
                mCircles.add(new CircleArea((int) circleAreaList.get(3).x, (int) circleAreaList.get(3).y, RADIOUS));
                invalidate();
            }
    
        }
    
        public List<CircleArea> getCurrentCoordinates() {
            return mCircles;
        }
    
        /**
         * Clears all CircleArea - pointer id relations
         */
        private void clearCirclePointer() {
            mCirclePointer.clear();
        }
    
        /**
         * Determines touched circle if exist
         * computing distance between center of each circle and touch center area
         *
         * @param xTouch int x touch coordinate
         * @param yTouch int y touch coordinate
         * @return {@link CircleArea} touched circle or null if no circle has been touched
         */
        private CircleArea getTouchedCircle(final int xTouch, final int yTouch) {
            CircleArea touched = null;
            for (CircleArea circle : mCircles) {
                if ((circle.centerX - xTouch) * (circle.centerX - xTouch) +
                        (circle.centerY - yTouch) * (circle.centerY - yTouch)
                        <= circle.radius * circle.radius + circle.radius * circle.radius + circle.radius * circle.radius) {
                    touched = circle;
                    break;
                }
            }
            return touched;
        }
    
        @Override
        public boolean onTouchEvent(MotionEvent event) {
    
            /**
             * flag for indicate user hold and drag touched circle
             */
            boolean handled = false;
    
            /**
             *Circle touched
             */
            CircleArea touchedCircle;
    
            int xTouch;
            int yTouch;
    
    
            switch (event.getActionMasked()) {
    
                case MotionEvent.ACTION_DOWN:
                    //Events fire whe user touch canvas
    
                    //clear points id's
                    clearCirclePointer();
    
                    // Default zero because we dont manage multitouch events.
                    xTouch = (int) event.getX(0);
                    yTouch = (int) event.getY(0);
    
                    //Determine touch circle
                    touchedCircle = getTouchedCircle(xTouch, yTouch);
    
                    if (touchedCircle != null) {
    
                        touchedCircle.centerX = xTouch;
                        touchedCircle.centerY = yTouch;
                        //add circle to list realationate with id event
                        mCirclePointer.put(event.getPointerId(0), touchedCircle);
                        // we indicate users are holding circle
                        handled = true;
                        invalidate();
                    } else {
                        handled = false;
                    }
                case MotionEvent.ACTION_POINTER_DOWN:
                    handled = true;
                    break;
    
                case MotionEvent.ACTION_MOVE:
                    int pointerId = 0;
                    final int pointerCount = event.getPointerCount();
                    int actionIndex = event.getActionIndex();
    
                    for (actionIndex = 0; actionIndex < pointerCount; actionIndex++) {
    
                        // Some pointer has moved, search it by pointer id
                        pointerId = event.getPointerId(actionIndex);
    
                        xTouch = (int) event.getX(actionIndex);
                        yTouch = (int) event.getY(actionIndex);
    
                        if ((xTouch > - RADIOUS && xTouch < drawCanvas.getWidth() + RADIOUS)
                                && (yTouch > - RADIOUS && yTouch < drawCanvas.getHeight() + RADIOUS)){
                            touchedCircle = mCirclePointer.get(pointerId);
    
                            if (null != touchedCircle) {
                                touchedCircle.centerX = xTouch;
                                touchedCircle.centerY = yTouch;
                            }
                        }
    
                    }
    
    
                    invalidate();
                    handled = true;
                    break;
                case MotionEvent.ACTION_UP:
                    clearCirclePointer();
                    handled = true;
                    break;
    
                case MotionEvent.ACTION_POINTER_UP:
                    clearCirclePointer();
                    handled = true;
                    break;
    
                case MotionEvent.ACTION_CANCEL:
                    clearCirclePointer();
                    handled = true;
                    break;
    
                default:
                    clearCirclePointer();
                    handled = true;
                    break;
            }
            return super.onTouchEvent(event) || handled;
        }
    
        /**
         * Stores data about single circle
         */
        public static class CircleArea {
            int radius;
            int centerX;
            int centerY;
    
            public CircleArea(int centerX, int centerY, int radius) {
                this.radius = radius;
                this.centerX = centerX;
                this.centerY = centerY;
            }
    
            public int getCenterX() {
                return centerX;
            }
    
    
            public int getCenterY() {
                return centerY;
            }
    
            @Override
            public String toString() {
                return "Circle[" + centerX + ", " + centerY + ", " + radius + "]";
            }
        }
    }
    