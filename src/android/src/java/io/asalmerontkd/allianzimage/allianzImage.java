package io.asalmerontkd.allianzimage;

import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import io.asalmerontkd.allianzimage.utils.Constants;

/**
 * This class echoes a string called from JavaScript.
 */
public class allianzImage extends CordovaPlugin {
    private CallbackContext callbackContext;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        try{
            if (action.equals("imageMethod")) {
                if (args.getString(0).length() > 0){
                    Constants.title = args.getString(0);
                }
                this.callbackContext = callbackContext;
                Intent ca = new Intent(this.cordova.getActivity().getApplicationContext(), CameraActivity.class);
                cordova.getActivity().startActivity(ca);
                PluginResult imageFinal = new PluginResult(PluginResult.Status.NO_RESULT);
                imageFinal.setKeepCallback(true);
                callbackContext.sendPluginResult(imageFinal);
                return true;
            }
            return false;
        }
        catch(Exception e){
            e.printStackTrace();
            PluginResult res = new PluginResult(PluginResult.Status.JSON_EXCEPTION);
            callbackContext.sendPluginResult(res);
            return false;
        }
    }
    
    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
        if (Constants.accept){
            Constants.accept=false;
            JSONObject jsonResult = new JSONObject();
            try{
                jsonResult.put("thumbImage", base64Transform(Constants.finalScaledBitmap));
                jsonResult.put("image", base64Transform(Constants.finalBitmap));
                PluginResult imageFinal = new PluginResult(PluginResult.Status.OK, jsonResult);
                imageFinal.setKeepCallback(false);
                callbackContext.sendPluginResult(imageFinal);
            }catch (JSONException e) {
                e.printStackTrace();
                PluginResult imageFinal = new PluginResult(PluginResult.Status.ERROR, "Error al cargar JSON.");
                imageFinal.setKeepCallback(false);
                callbackContext.sendPluginResult(imageFinal);
            }
            
        }
        else
        {
            PluginResult imageFinal = new PluginResult(PluginResult.Status.ERROR, "Evento cancelado.");
            imageFinal.setKeepCallback(false);
            callbackContext.sendPluginResult(imageFinal);
        }
    }

    public String base64Transform(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }
}
