package io.asalmerontkd.allianzimage;

import android.app.Dialog;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import io.asalmerontkd.allianzimage.utils.Constants;
import io.asalmerontkd.allianzimage.utils.VerticalTextView;

public class ResultCrop extends AppCompatActivity {
    ImageView imageResult;
    Mat img;


    ImageButton btnControlBack;
    ImageButton btnControlAccept;
    ImageButton btnControlRotate;
    private VerticalTextView titleVertical;

    Bitmap myBitmap;
    Bitmap myScaledBitmap;
    private Dialog loadDialog;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources().getIdentifier("activity_result", "layout", getPackageName()));
        toolbar = (Toolbar) findViewById(getResources().getIdentifier("toolbar", "id", getPackageName()));
        setSupportActionBar(toolbar);
        imageResult = (ImageView) findViewById(getResources().getIdentifier("imageResult", "id", getPackageName()));
        titleVertical = (VerticalTextView) findViewById(getResources().getIdentifier("verticalTitle", "id", getPackageName()));
        btnControlBack = (ImageButton) findViewById(getResources().getIdentifier("resultControlBack", "id", getPackageName()));
        btnControlAccept = (ImageButton) findViewById(getResources().getIdentifier("resultControlAccept", "id", getPackageName()));
        btnControlRotate = (ImageButton) findViewById(getResources().getIdentifier("resultControlRotate", "id", getPackageName()));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Constants.title);
        titleVertical.setText(Constants.title);
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
        initThings();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.i(Constants.TAG, "LANDSCAPE");
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Log.i(Constants.TAG, "PORTRAIT");
        }
    }

    private void initThings()
    {
        loadDialog = new Dialog(this);
        loadDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loadDialog.setCancelable(false);
        loadDialog.setContentView(getResources().getIdentifier("load_dialog", "layout", getPackageName()));
        loadDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        btnControlBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnControlRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Rotar().execute();
            }
        });
        btnControlAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.accept = true;
                Constants.finalBitmap = myBitmap;
                Constants.finalScaledBitmap = myScaledBitmap;
                Log.e("accept","myBitmap Width: " + myBitmap.getWidth() + " - Heigth: " + myBitmap.getHeight());
                Log.e("accept","myScaledBitmap Width: " + myScaledBitmap.getWidth() + " - Heigth: " + myScaledBitmap.getHeight());
                finish();
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public class Rotar extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            Matrix matrix = new Matrix();
            matrix.preRotate(90);
            myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
            myScaledBitmap = escalarImagen(myBitmap, 150, 150);
            return myBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageResult.setImageBitmap(bitmap);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Constants.imgResult == null){
            finish();
        }
        else{
            img = new Mat();
            img = Constants.imgResult;
            new LoadResultImage().execute();
        }
    }

    public class LoadResultImage extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... params) {
            int w = 0, h = 0, scaletmp = 0;
            myBitmap = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(img,myBitmap);
            if(img.cols() > 1024 || img.rows() > 1024){
                if(img.cols() > 1024){
                    scaletmp = (int) (102400 / img.cols());
                    w = 1024;
                    h = (int) ((scaletmp * img.rows()) / 100);
                }
                if(img.rows() > 1024){
                    scaletmp = (int) (102400 / img.rows());
                    h = 1024;
                    w = (int) ((scaletmp * img.cols()) / 100);
                }
                myBitmap = escalarImagen(myBitmap, w, h);
                Utils.bitmapToMat(myBitmap, img);
            }
            myScaledBitmap = escalarImagen(myBitmap, 150, 150);
            return myBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.e("Main","Finish");
            super.onPostExecute(bitmap);
            imageResult.setImageBitmap(bitmap);
        }
    }

    public Bitmap escalarImagen(Bitmap imagen, int scaleW, int scaleH){
        int width = imagen.getWidth();
        int height = imagen.getHeight();
        float scaleWidth = ((float) scaleW) / width;
        float scaleHeight = ((float) scaleH) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(imagen, 0, 0, width, height, matrix, false);
    }
}
