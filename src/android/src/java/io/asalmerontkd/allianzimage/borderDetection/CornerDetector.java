package io.asalmerontkd.allianzimage.borderDetection;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;


import io.asalmerontkd.allianzimage.utils.Constants;

/**
 * Created by asalmerontkd on 23/06/17.
 */

public class CornerDetector {
    Mat img;

    public CornerDetector(Bitmap bmp) {
        img = new Mat();
        Utils.bitmapToMat(bmp, img);
    }

    private List<MatOfPoint> contourBubbleSort(List<MatOfPoint> contours)
    {
        boolean swaped;
        for(int i= 0; i < contours.size()-1; i++){
            swaped=false;
            for(int j=0;j <contours.size()-1;j++){
                if ((Imgproc.contourArea(contours.get(j))) < (Imgproc.contourArea(contours.get(j+1)))){
                    List<MatOfPoint> aux_vector = new ArrayList<MatOfPoint>();
                    aux_vector.add(contours.get(j+1));
                    contours.set(j+1, contours.get(j));
                    contours.set(j, aux_vector.get(0));
                    swaped=true;
                }
            }
            if(swaped==false)
                break;
        }
        return contours;
    }

    public ArrayList<Point>  getPoints(){
        // Image resize
        float val_magic=400;
        float relation;
        relation=val_magic/img.cols();
        Imgproc.resize(img, img,new Size(val_magic, img.rows()*relation), 0, 0, Imgproc.INTER_LINEAR);

        // Image preprocessing
        Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
        Imgproc.medianBlur(img, img, 9);

        int[] areas = new int[3];
        List<MatOfPoint2f> screenCnts = new ArrayList();
        for(int level= 0; level < 3; level++){
            if (level == 0){
                Imgproc.Canny(img, img, 10, 20, 3, true);
                Imgproc.dilate(img, img, new Mat(), new Point(-1, -1), 1);
            }
            else if (level == 1){
                Imgproc.threshold(img, img, 100, 255, Imgproc.THRESH_BINARY);
                Imgproc.Canny(img, img, 10, 20, 3, true);
                Imgproc.dilate(img, img, new Mat(), new Point(-1, -1), 1);
            }
            else if (level == 2){
                Imgproc.threshold(img, img, 180, 255, Imgproc.THRESH_BINARY);
                Imgproc.Canny(img, img, 10, 20, 3, true);
                Imgproc.dilate(img, img, new Mat(), new Point(-1, -1), 1);
            }

            // Find contours
            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            Mat hierarchy = new Mat();
            Imgproc.findContours(img, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

            contours=contourBubbleSort(contours);
            MatOfPoint2f screenCnt = new MatOfPoint2f();
            MatOfPoint temp_contour = new MatOfPoint();

            for (int idx = 0; idx < contours.size(); idx++) {
                temp_contour = contours.get(idx);

                double area_act = Imgproc.contourArea(contours.get(idx));

                double perimeter= Imgproc.arcLength(new MatOfPoint2f(temp_contour.toArray()), true);
                MatOfPoint2f new_mat = new MatOfPoint2f(temp_contour.toArray());
                MatOfPoint2f approxCurve_temp = new MatOfPoint2f();
                Imgproc.approxPolyDP(new_mat, approxCurve_temp, perimeter * 0.05, true);

                if (approxCurve_temp.total() == 4)
                {
                    screenCnt = approxCurve_temp;
                    areas[level] = (int) area_act;
                    screenCnts.add(screenCnt);
                    Log.e("CornerDetector","founded value on level " + level + "  with area: " + area_act);
                    break;
                }
                if (idx == contours.size() - 1) {
                    screenCnt = approxCurve_temp;
                    areas[level]=0;
                    screenCnts.add(screenCnt);
                    Log.e("CornerDetector","Skiped level " + level);
                }
            }
        }

        int borderInd = 0;
        int N = 0;
        for (int i = 0; i<areas.length; i++){
            Log.e("CORNERDETECTOR","Area: " + areas[i] + " IDX: " + i);
            if (areas[i]>N){
                N = areas[i];
                borderInd = i;
            }
        }

        MatOfPoint2f screenCnt = new MatOfPoint2f();
        Point p1;
        Point p2;
        Point p3;
        Point p4;
        if (N==0){
            p1 = new Point(1,1);
            p2 = new Point(2,2);
            p3 = new Point(3,3);
            p4 = new Point(4,4);
        }
        else{
            screenCnt = screenCnts.get(borderInd);
            double[] temp_double;
            temp_double = screenCnt.get(0, 0);
            p1 = new Point(temp_double[0] * 1 / relation, temp_double[1] * 1 / relation);
            temp_double = screenCnt.get(1, 0);
            p2 = new Point(temp_double[0] * 1 / relation, temp_double[1] * 1 / relation);
            temp_double = screenCnt.get(2, 0);
            p3 = new Point(temp_double[0] * 1 / relation, temp_double[1] * 1 / relation);
            temp_double = screenCnt.get(3, 0);
            p4 = new Point(temp_double[0] * 1 / relation, temp_double[1] * 1 / relation);
        }

        ArrayList<Point> source = new ArrayList<Point>();
        ArrayList<Point> myPoints = new ArrayList<Point>();

        source.add(p1);
        source.add(p2);
        source.add(p3);
        source.add(p4);


        myPoints.add(source.get(0));
        myPoints.add(source.get(1));
        myPoints.add(source.get(2));
        myPoints.add(source.get(3));

        return myPoints;
    }
}
