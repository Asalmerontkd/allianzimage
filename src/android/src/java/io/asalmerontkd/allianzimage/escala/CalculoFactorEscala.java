package io.asalmerontkd.allianzimage.escala;

import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * Created by asalmerontkd on 29/06/17.
 */

public class CalculoFactorEscala {
    int ow;
    int oh;
    public CalculoFactorEscala(int ow, int oh){
        this.ow = ow;
        this.oh = oh;
        //ow -> original width
        //oh -> orginal height
    }

    public void setOw(int ow) {
        this.ow = ow;
    }

    public void setOh(int oh) {
        this.oh = oh;
    }

    public ArrayList<Point> calcularPuntos(ArrayList<Point> points, int fw, int fh, boolean mayorAMenor){
        double factorX, factorY;

        int puntoX, puntoY;
        Point punto;

        ArrayList<Point> puntosFinales = new ArrayList();

        factorX = (double)ow/fw;//si va de menor a mayor, se dvide el final entre el original
        factorY = (double)oh/fh;

        for(Point point : points){

            if(mayorAMenor){

                puntoX = (int) (point.x/factorX);//Se divide si va de una pantalla mayor a una pantalla menor
                puntoY = (int) (point.y/factorY);

            }else{

                puntoX = (int) (point.x*factorX);//Se multiplica si va de una pantalla menor a una pantalla mayor
                puntoY = (int) (point.y*factorY);

            }
            punto = new Point(puntoX, puntoY);
            puntosFinales.add(punto);

        }

        return puntosFinales;

    }
}
