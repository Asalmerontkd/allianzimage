package io.asalmerontkd.allianzimage;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.media.ExifInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.picasso.Picasso;

import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import io.asalmerontkd.allianzimage.borderDetection.CornerDetector;
import io.asalmerontkd.allianzimage.escala.CalculoFactorEscala;
import io.asalmerontkd.allianzimage.perspectiveCorrection.PerspectiveCorrection;
import io.asalmerontkd.allianzimage.ui.bordes.ImageViewAdjustBorders;
import io.asalmerontkd.allianzimage.utils.AreaImagen;
import io.asalmerontkd.allianzimage.utils.Constants;

public class ShowImageActivity extends AppCompatActivity {
    
        private FrameLayout frameLayout;
        private ImageViewAdjustBorders imageViewAdjustBorders;
    
        private CornerDetector border;
        private ArrayList<Point> puntos;
        private ArrayList<Point> corners;
        private CalculoFactorEscala calculoFactorEscala;
        private AreaImagen areaImagen;
        private Bitmap bitmapFrame;
    
        private ImageButton controlAccept;
        private Dialog loadDialog;
        private Toolbar toolbar;
    
        LinearLayout.LayoutParams lp;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(getResources().getIdentifier("activity_show_image", "layout", getPackageName()));
    
            toolbar = (Toolbar) findViewById(getResources().getIdentifier("toolbar", "id", getPackageName()));
            setSupportActionBar(toolbar);
            Constants.fileName = getIntent().getStringExtra("fileName");
            frameLayout = (FrameLayout) findViewById(getResources().getIdentifier("frame", "id", getPackageName()));
    
            
            //controlAccept = (ImageButton) findViewById(getResources().getIdentifier("showControlAccept", "id", getPackageName()));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(Constants.title);
            toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
            initThings();
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(getResources().getIdentifier("menu_accept", "menu", getPackageName()), menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            Log.e(Constants.TAG, "MenuItem: " + item);
            Log.e(Constants.TAG, "MenuItem id: " + id);
            puntos = new ArrayList();
            if (imageViewAdjustBorders != null) {
                if (imageViewAdjustBorders.getCurrentCoordinates() == null || imageViewAdjustBorders.getCurrentCoordinates().isEmpty()) {
                    //TODO si no hay circulos hacer algo aquí

                } else {
                    for (ImageViewAdjustBorders.CircleArea circle : imageViewAdjustBorders.getCurrentCoordinates()) {
                        puntos.add(new Point(circle.getCenterX(), circle.getCenterY()));
                    }
                    new Perspective().execute();
                }
            }

            return super.onOptionsItemSelected(item);
        }
    
        private void initThings()
        {
            /*
            controlAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    puntos = new ArrayList();
                    if (imageViewAdjustBorders != null) {
                        if (imageViewAdjustBorders.getCurrentCoordinates() == null || imageViewAdjustBorders.getCurrentCoordinates().isEmpty()) {
                            //TODO si no hay circulos hacer algo aquí
    
                        } else {
                            for (ImageViewAdjustBorders.CircleArea circle : imageViewAdjustBorders.getCurrentCoordinates()) {
                                puntos.add(new Point(circle.getCenterX(), circle.getCenterY()));
                            }
                            new Perspective().execute();
                        }
                    }
                }
            });*/
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            calculoFactorEscala = new CalculoFactorEscala(Constants.originalImageWidh, Constants.originalImageHeight);
            areaImagen = new AreaImagen();
            loadDialog = new Dialog(this);
            loadDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            loadDialog.setCancelable(false);
            loadDialog.setContentView(getResources().getIdentifier("load_dialog", "layout", getPackageName()));
            loadDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
    
        @Override
        protected void onResume() {
            super.onResume();
            if (Constants.accept){
                finish();
            }
        }
    
        public class Perspective extends AsyncTask<String, Void, String> {
            Mat mat;
            PerspectiveCorrection perspectiveCorrection;
            @Override
            protected String doInBackground(String... params) {
                puntos = calculoFactorEscala.calcularPuntos(puntos, Constants.frameWidh, Constants.frameHeight, false);
                perspectiveCorrection = new PerspectiveCorrection(bitmapFrame, puntos);
                mat = new Mat();
                mat = perspectiveCorrection.correctPerspective();
                return "ok";
            }
    
            @Override
            protected void onPostExecute(String s) {
                Constants.imgResult = new Mat();
                Constants.imgResult = mat;
                Intent i = new Intent(ShowImageActivity.this, ResultCrop.class);
                startActivity(i);
                finish();
                super.onPostExecute(s);
            }
        }
    
        public class Corners extends AsyncTask<String,String, ArrayList<Point>>{
    
            @Override
            protected ArrayList<Point>  doInBackground(String... strings) {
                border = new CornerDetector(bitmapFrame);
                corners = calculoFactorEscala.calcularPuntos(border.getPoints(), Constants.frameWidh, Constants.frameHeight, true);
                return corners;
            }
    
            @Override
            protected void onPostExecute(ArrayList<Point>  corn) {
                super.onPostExecute(corn);
                if (!areaImagen.escalaCorrecta(corn, Constants.frameWidh, Constants.frameHeight)){
                    corn.clear();
                    Log.e(Constants.TAG, "Area muy pequeña, ingresando valores por default!!!");
                }
                imageViewAdjustBorders.setCirclesBorders(corn);
                dimissLoadDialog();
            }
        }
    
        @Override
        protected void onStart() {
            super.onStart();
            frameLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (Constants.frameWidh == 0 || Constants.frameHeight == 0)
                    {
                        Constants.frameWidh = frameLayout.getWidth();
                        Constants.frameHeight = frameLayout.getHeight();
                    }
                    if (Constants.file == null) {
                        finish();
                    }
                    else{
                        if (!Constants.accept){
                            new InitFrames().execute();
                        }
                    }
    
                }
            });
        }
    
        public class InitFrames extends AsyncTask<String, String, Bitmap>{
            @Override
            protected Bitmap doInBackground(String... strings) {
                showLoadDialog();
                Bitmap bg = null;
                bitmapFrame = decodeFile(new File(Constants.fileName), 800);
                try {
    
                    Matrix matrix = new Matrix();
                    int orientation = getImageRotation(Constants.fileName);
    
                    matrix.preRotate(orientation != -1 ? orientation : 0);
                    bitmapFrame = Bitmap.createBitmap(bitmapFrame, 0, 0, bitmapFrame.getWidth(), bitmapFrame.getHeight(), matrix, true);
                    Constants.originalImageWidh = bitmapFrame.getWidth();
                    Constants.originalImageHeight = bitmapFrame.getHeight();
                    calculoFactorEscala.setOh(Constants.originalImageHeight);
                    calculoFactorEscala.setOw(Constants.originalImageWidh);
                    Constants.uriImage = getImageUri(bitmapFrame);
                    bg = Picasso.with(ShowImageActivity.this).load(Constants.uriImage)
                            .resize(Constants.frameWidh, 0)
                            .get();
    
                    bg = Picasso.with(ShowImageActivity.this).load(Constants.uriImage)
                            .resize(bg.getWidth(), bg.getHeight())
                            .get();
                    lp = new LinearLayout.LayoutParams(bg.getWidth(), bg.getHeight());
                    Constants.frameHeight = bg.getHeight();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return bg;
            }
    
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                frameLayout.setLayoutParams(lp);
                imageViewAdjustBorders = new ImageViewAdjustBorders(ShowImageActivity.this, bitmap);
                imageViewAdjustBorders.setRADIOUS(getResources().getInteger(getResources().getIdentifier("radious", "integer", getPackageName())));
                frameLayout.addView(imageViewAdjustBorders);
                new Corners().execute();
            }
        }
    
    
        /**
         * @param imagePath
         * @return
         */
        public static int getImageRotation(String imagePath) {
            try {
                File imageFile = new File(imagePath);
                if (imageFile.exists()) {
                    ExifInterface exif = new ExifInterface(imageFile.getPath());
                    int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                    return exifToDegrees(rotation);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return -1;
        }
    
        /**
         * @param exifOrientation
         * @return
         */
        private static int exifToDegrees(int exifOrientation) {
            if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
                return 90;
            } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
                return 180;
            } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
                return 270;
            }
            return 0;
        }
    
        public Uri getImageUri(Bitmap inImage) {
            FileOutputStream fos = null;
            try {
                Constants.fileBackgroud = new File(getCacheDir(), "picture2.jpg");
                Constants.fileBackgroud.createNewFile();

    //Convert bitmap to byte array
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                byte[] bitmapdata = bytes.toByteArray();

    //write the bytes in file
                fos = new FileOutputStream(Constants.fileBackgroud);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            } catch (IOException e) {
                Log.e(Constants.TAG, "Cannot write to " + Constants.file, e);
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        // Ignore
                        Log.e(Constants.TAG, "Error Exception OS", e);
                    }
                }
            }
            return Uri.parse("file://" +Constants.fileBackgroud.getPath());
        }
    
        /**
         * @param file
         * @param requiredHeight
         * @return
         */
        public static Bitmap decodeFile(File file, int requiredHeight) {
            try {
                // Decode image size
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, o);
                if (bitmap != null) {
                    return bitmap;
                }
                // Find the correct scale value. It should be the power of 2.
                int scale = 1;
                while (o.outWidth / scale / 2 >= requiredHeight &&
                        o.outHeight / scale / 2 >= requiredHeight) {
                    scale *= 2;
                }
                // Decode with inSampleSize
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                return BitmapFactory.decodeStream(new FileInputStream(file), null, o2);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }
    
        private void showLoadDialog(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!loadDialog.isShowing()){
                        loadDialog.show();
                    }
                }
            });
        }
    
        private void dimissLoadDialog(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (loadDialog.isShowing()){
                        loadDialog.dismiss();
                    }
                }
            });
        }
    }
    