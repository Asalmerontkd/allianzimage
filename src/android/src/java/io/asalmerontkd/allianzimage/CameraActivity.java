package io.asalmerontkd.allianzimage;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.cameraview.AspectRatio;
import com.google.android.cameraview.CameraView;

import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.asalmerontkd.allianzimage.utils.AspectRatioFragment;
import io.asalmerontkd.allianzimage.utils.Constants;
import io.asalmerontkd.allianzimage.utils.VerticalTextView;

public class CameraActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, AspectRatioFragment.Listener {
    
    
        private int mCurrentFlash;
        private CameraView mCameraView;
        private Handler mBackgroundHandler;
        private ImageButton btnTakePicture;
        private Dialog loadDialog;
        private Toolbar toolbar;
        private VerticalTextView titleVertical;
    
        public int[] FLASH_ICONS;
    
        static {
            if (!OpenCVLoader.initDebug()) {
                Log.e(Constants.TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            } else {
                Log.e(Constants.TAG, "OpenCV library found inside package. Using it!");
            }
        }
    
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(getResources().getIdentifier("activity_camera", "layout", getPackageName()));
            toolbar = (Toolbar) findViewById(getResources().getIdentifier("toolbar", "id", getPackageName()));
            setSupportActionBar(toolbar);
            titleVertical = (VerticalTextView) findViewById(getResources().getIdentifier("verticalTitle", "id", getPackageName()));
            mCameraView = (CameraView) findViewById(getResources().getIdentifier("cameraView", "id", getPackageName()));
            btnTakePicture = (ImageButton) findViewById(getResources().getIdentifier("camBtnTakePicture", "id", getPackageName()));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(Constants.title);
            titleVertical.setText(Constants.title);
            toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));
    
            initThings();
        }
    
        @Override
        public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
            // Checks the orientation of the screen
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                Log.i(Constants.TAG, "LANDSCAPE");
            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
                Log.i(Constants.TAG, "PORTRAIT");
            }
        }
    
        private void initThings() {
            FLASH_ICONS = new int[3];
            FLASH_ICONS[0] = getResources().getIdentifier("ic_flash_auto", "drawable", getPackageName());
            FLASH_ICONS[1] = getResources().getIdentifier("ic_flash_off", "drawable", getPackageName());
            FLASH_ICONS[2] = getResources().getIdentifier("ic_flash_on", "drawable", getPackageName());
            if (mCameraView != null) {
                mCameraView.addCallback(mCallback);
            }
            btnTakePicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mCameraView != null) {
                        btnTakePicture.setClickable(false);
                        showLoadDialog();
                        mCameraView.takePicture();
                    }
                }
            });
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            loadDialog = new Dialog(this);
            loadDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            loadDialog.setCancelable(false);
            loadDialog.setContentView(getResources().getIdentifier("load_dialog", "layout", getPackageName()));
            loadDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
    
        @Override
        protected void onResume() {
            super.onResume();
            if (Constants.accept){
                finish();
            }
            if (requestPermission()) {
                mCameraView.start();// or we can  do any action
            }
    
        }
    
        @Override
        protected void onPause() {
            mCameraView.stop();
            super.onPause();
        }
    
        @Override
        protected void onDestroy() {
            super.onDestroy();
            if (mBackgroundHandler != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    mBackgroundHandler.getLooper().quitSafely();
                } else {
                    mBackgroundHandler.getLooper().quit();
                }
                mBackgroundHandler = null;
            }
        }
    
    
    
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(getResources().getIdentifier("main", "menu", getPackageName()), menu);
            menu.findItem(getResources().getIdentifier("switch_flash", "id", getPackageName())).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            return true;
        }
    
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if (item.getItemId() == getResources().getIdentifier("switch_flash", "id", getPackageName())){
                if (mCameraView != null) {
                    mCurrentFlash = (mCurrentFlash + 1) % Constants.FLASH_OPTIONS.length;
                    item.setTitle(Constants.FLASH_TITLES[mCurrentFlash]);
                    item.setIcon(FLASH_ICONS[mCurrentFlash]);
                    mCameraView.setFlash(Constants.FLASH_OPTIONS[mCurrentFlash]);
                }
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    
        @Override
        public void onAspectRatioSelected(@NonNull AspectRatio ratio) {
            if (mCameraView != null) {
                Toast.makeText(this, ratio.toString(), Toast.LENGTH_SHORT).show();
                mCameraView.setAspectRatio(ratio);
            }
        }
    
        private Handler getBackgroundHandler() {
            if (mBackgroundHandler == null) {
                HandlerThread thread = new HandlerThread("background");
                thread.start();
                mBackgroundHandler = new Handler(thread.getLooper());
            }
            return mBackgroundHandler;
        }
    
        private CameraView.Callback mCallback = new CameraView.Callback() {
    
            @Override
            public void onCameraOpened(CameraView cameraView) {
                Log.d(Constants.TAG, "onCameraOpened");
            }
    
            @Override
            public void onCameraClosed(CameraView cameraView) {
                Log.d(Constants.TAG, "onCameraClosed");
            }
    
            @Override
            public void onPictureTaken(final CameraView cameraView, final byte[] data) {
                getBackgroundHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        //String file_path = Environment.getExternalStorageDirectory().getAbsolutePath();
                        Constants.file = new File(getCacheDir(), "picture.jpg");
                        FileOutputStream os = null;
                        try {
                            Constants.file.createNewFile();
                            os = new FileOutputStream(Constants.file);
                            os.write(data);
                            os.close();
                        } catch (IOException e) {
                            Log.e(Constants.TAG, "Cannot write to " + Constants.file, e);
                        } finally {
                            if (os != null) {
                                try {
                                    os.close();
                                } catch (IOException e) {
                                    // Ignore
                                    Log.e(Constants.TAG, "Error Exception OS", e);
                                }
                            }
                        }
                        startActivity(new Intent(CameraActivity.this, ShowImageActivity.class).putExtra("fileName", Constants.file.getPath()));
                        dimissLoadDialog();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnTakePicture.setClickable(true);
                            }
                        });
                    }
                });
            }
    
        };
    
        private void showLoadDialog(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!loadDialog.isShowing()){
                        loadDialog.show();
                    }
                }
            });
        }
    
        private void dimissLoadDialog(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (loadDialog.isShowing()){
                        loadDialog.dismiss();
                    }
                }
            });
        }
    
        /**
         * Request all necesary permission
         *
         * @return true if we have all permission Granted
         */
        public boolean requestPermission() {
            // Get permission status
            int writeStorageAudiopermission =
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int readStoragePermission =
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            int cameraPermission =
                    ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
    
            //List will have all permission no Granted
            List<String> listPermissionsNeeded = new ArrayList();
    
            /**
             * verify if we have necesary permission
             */
            if (writeStorageAudiopermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            // if we have permission no Granted, we have to request necesary permission
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                        Constants.REQUEST_PERMMISSION_STATUS);
                return false;
            }
            return true;
        }
    
    
        /**
         * Method that is sent automatically call the response requested permissions
         *
         * @param requestCode
         * @param permissions
         * @param grantResults
         */
        @Override
        public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    
            if (Constants.REQUEST_PERMMISSION_STATUS == requestCode) {
    
                Map<String, Integer> perms = new HashMap();
                // Initialize the map with both permissions
    
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
    
    
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++) {
                        perms.put(permissions[i], grantResults[i]);
                    }
    
    
                    // Check for all permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        // we have permission for use this permissions
                        //TODO do something
                        mCameraView.start();// or we can  do any action
                    } else {
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            // request again
                            requestPermission();
    
                        } else {
                            // request again
                            Toast.makeText(CameraActivity.this, "Tienes que agregar los permisos manualmente.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
    
                }
    
            }
        }
    }
    