package io.asalmerontkd.allianzimage.perspectiveCorrection;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

import io.asalmerontkd.allianzimage.utils.OrderPoints;

/**
 * Created by asalmerontkd on 23/06/17.
 */

public class PerspectiveCorrection {
    Mat imSrc;
    ArrayList<Point> source;
    Point[] coordinates;
    OrderPoints orderPoints;
    Size size;
    public PerspectiveCorrection(Bitmap bmp, ArrayList<Point> points){
        this.source = points;
        this.imSrc = new Mat();
        Utils.bitmapToMat(bmp, imSrc);
        orderPoints = new OrderPoints();
    }

    public static double maxTD(Point[] coordinates) {
        double topDistX = Math.pow(coordinates[1].x - coordinates[0].y, 2);
        double topDistY =  Math.pow(coordinates[1].y - coordinates[0].y, 2);
        double topDist = topDistX + topDistY;
        double downDistX =  Math.pow(coordinates[3].x - coordinates[2].x, 2);
        double downDistY =  Math.pow(coordinates[3].y - coordinates[2].y, 2);
        double downDist = downDistX + downDistY;
        if (topDist > downDist) {
            return topDist;
        } else {
            return downDist;
        }
    }

    public static double maxLR(Point[] coordinates) {
        double rightDistX =  Math.pow(coordinates[2].x - coordinates[1].x, 2);
        double rightDistY =  Math.pow(coordinates[2].y - coordinates[1].y, 2);
        double rightDist = rightDistX + rightDistY;
        double leftDistX =  Math.pow(coordinates[0].x - coordinates[3].x, 2);
        double leftDistY =  Math.pow(coordinates[0].y - coordinates[3].y, 2);
        double leftDist = leftDistX + leftDistY;
        if (rightDist > leftDist) {
            return rightDist;
        } else {
            return leftDist;
        }
    }

    public Mat correctPerspective(){
        coordinates = orderPoints.sortPoints(source);

        double height = Math.sqrt(maxLR(coordinates));
        double width = Math.sqrt(maxTD(coordinates));
        System.out.println("height: " + (height));
        System.out.println("width: " + (width));
        size = new Size(width, height);

        // Create Point objects
        MatOfPoint2f sourcePoints = new MatOfPoint2f();
        Point[] destPoints = new Point[4];
        sourcePoints.fromArray(coordinates);
        destPoints[3] = new Point(0, 0);
        destPoints[2] = new Point(size.width - 1, 0);
        destPoints[1] = new Point(size.width - 1, size.height - 1);
        destPoints[0] = new Point(0, size.height - 1);

        // Find homography
        MatOfPoint2f dest = new MatOfPoint2f();
        dest.fromArray(destPoints);
        Mat h = Calib3d.findHomography(sourcePoints, dest);

        // Warp source image to destination
        Mat imDest = new Mat().zeros(size, CvType.CV_8SC3);
        Imgproc.warpPerspective(imSrc, imDest, h, size);

        // Show imDest

        return imDest;
    }
}
