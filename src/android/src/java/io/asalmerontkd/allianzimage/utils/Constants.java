package io.asalmerontkd.allianzimage.utils;

import android.graphics.Bitmap;

import com.google.android.cameraview.CameraView;

import org.opencv.core.Mat;

import java.io.File;

import android.net.Uri;

/**
 * Created by asalmerontkd on 22/06/17.
 */

public class Constants {

        public static int x ;
        public static int y ;
        public static int alto;
        public static int ancho;
        public static int rejilla ;
        public static int constanteX ;
        public static int constanteY ;

        public static String title = "Allianz";

        public static boolean accept = false;
        public static Bitmap finalBitmap = null;
        public static Bitmap finalScaledBitmap = null;

        public static Uri uriImage = null;
        public static String TAG = "AllianzImagen";
        public static String fileName = "";
        public static File file;
        public static File fileBackgroud;
        public static Mat imgResult;

        public static int frameWidh = 0;
        public static int frameHeight = 0;

        public static int originalImageWidh = 0;
        public static int originalImageHeight = 0;

        public static final int[] FLASH_OPTIONS = {
                CameraView.FLASH_AUTO,
                CameraView.FLASH_OFF,
                CameraView.FLASH_ON,
        };

        public static final String[] FLASH_TITLES = {
                "Flash auto",
                "Flash off",
                "Flash on",
        };

        public static final int REQUEST_PERMMISSION_STATUS = 150;
}
