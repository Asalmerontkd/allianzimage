package io.asalmerontkd.allianzimage.utils;

import android.util.Log;

import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * Created by asalmerontkd on 7/07/17.
 */

public class AreaImagen {
    public AreaImagen(){

    }

    public double getAreaPoints(ArrayList<Point> points){
        double area = 0;
        if (points.size()>0)
        {
            Point p1 = points.get(0);
            Point p2 = points.get(1);
            Point p3 = points.get(2);
            Point p4 = points.get(3);
            area = ((p1.x * p2.y) + (p2.x * p3.y) + (p3.x * p4.y) + (p4.x * p1.y)) -
                    ((p2.x * p1.y) + (p3.x * p2.y) + (p4.x * p3.y) + (p1.x * p4.y));
            area = Math.abs(area) / 2;
        }
        else {
            return 0;
        }
        return area;
    }

    public int getAreaImagen(int ancho, int alto){
        return ancho * alto;
    }

    public boolean escalaCorrecta(ArrayList<Point> points, int ancho, int alto){
        double areaPuntos = getAreaPoints(points);
        int areaImagen = getAreaImagen(ancho, alto);
        int proporcion = areaImagen / 10;
        Log.e("areImagen", "areaPuntos: "+ areaPuntos);
        Log.e("areImagen", "areaImagen: "+ areaImagen);
        Log.e("areImagen", "proporcion: "+ proporcion);
        if (areaPuntos >= proporcion){
            return true;
        }
        return false;
    }
}
