package io.asalmerontkd.allianzimage.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Ken on 22/05/17.
 */

public class CirclesView extends View {

    public CirclesView(Context context) {
        super(context);
    }

    public CirclesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CirclesView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
