package io.asalmerontkd.allianzimage.utils;

import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by asalmerontkd on 5/07/17.
 */

public class OrderPoints {

    public Point[] sortPoints(ArrayList<Point> points){
        ArrayList<Point> topPoints = new ArrayList<Point>();
        ArrayList<Point> bottomPoints = new ArrayList<Point>();

        Collections.sort(points, new Comparator<Point>() {

            public int compare(Point o1, Point o2) {
                return Double.compare(o1.y, o2.y);
            }
        });

        topPoints.add(points.get(0));
        topPoints.add(points.get(1));

        Collections.sort(topPoints, new Comparator<Point>() {

            public int compare(Point o1, Point o2) {
                return Double.compare(o1.x, o2.x);
            }
        });



        bottomPoints.add(points.get(2));
        bottomPoints.add(points.get(3));

        Collections.sort(bottomPoints, new Comparator<Point>() {

            public int compare(Point o1, Point o2) {
                return Double.compare(o1.x, o2.x);
            }
        });
        Point[] puntos = new Point[4];
        puntos[3] = topPoints.get(0);
        puntos[2] = topPoints.get(1);
        puntos[1] = bottomPoints.get(1);
        puntos[0] = bottomPoints.get(0);
        return puntos;
    }
}
