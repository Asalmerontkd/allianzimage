var exec = require('cordova/exec');

exports.imageMethod = function(arg0, success, error) {
    exec(success, error, "allianzImage", "imageMethod", [arg0]);
};
